﻿namespace Mediator
{
    public class Participant
    {
        private readonly Mediator _mediator;

        public Participant(Mediator mediator)
        {
            _mediator = mediator;
            mediator.Join(this);
        }

        public int Value { get; set; }

        public void Say(int n)
        {
            _mediator.Broadcast(this, n);
        }

        public override string ToString()
        {
            return $"{nameof(Value)}: {Value}";
        }
    }
}