﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mediator
{
    public class Mediator
    {
        private readonly List<Participant> _participants = new List<Participant>();

        public void Join(Participant participant)
        {
            _participants.Add(participant);
        }

        public void Broadcast(Participant sender, int value)
        {
            foreach (var participant in _participants.Where(participant => participant != sender))
                participant.Value += value;
        }

        public override string ToString()
        {
            var results = _participants.Select((participant, i) => $"{i + 1}: {participant}");

            return string.Join(Environment.NewLine, results);
        }
    }
}