﻿using System;

namespace Mediator
{
    internal static class Program
    {
        private static void Main()
        {
            var mediator = new Mediator();

            var participant1 = new Participant(mediator);
            var participant2 = new Participant(mediator);

            participant1.Say(2);
            Console.WriteLine(mediator);
            Console.WriteLine();

            participant2.Say(4);
            Console.WriteLine(mediator);
        }
    }
}