﻿using System.Text;

namespace Visitor
{
    public class ExpressionPrinter : ExpressionVisitor
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        public override void Visit(Value value)
        {
            _stringBuilder.Append(value.TheValue);
        }

        public override void Visit(AdditionExpression expression)
        {
            _stringBuilder.Append('(');
            expression.LHS.Accept(this);
            _stringBuilder.Append('+');
            expression.RHS.Accept(this);
            _stringBuilder.Append(')');
        }

        public override void Visit(MultiplicationExpression expression)
        {
            expression.LHS.Accept(this);
            _stringBuilder.Append('*');
            expression.RHS.Accept(this);
        }

        public override string ToString()
        {
            return _stringBuilder.ToString();
        }
    }
}