﻿namespace Visitor
{
    public class Value : Expression
    {
        public readonly int TheValue;

        public Value(int value)
        {
            TheValue = value;
        }

        public override void Accept(ExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}