﻿using System;

namespace Visitor
{
    internal static class Program
    {
        private static void Main()
        {
            var expression = new MultiplicationExpression(
                new AdditionExpression(new Value(2), new Value(3)),
                new AdditionExpression(new Value(4), new Value(5))
            );

            var printer = new ExpressionPrinter();
            printer.Visit(expression);

            Console.WriteLine(printer);
        }
    }
}