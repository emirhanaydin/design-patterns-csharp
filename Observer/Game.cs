﻿using System;

namespace Observer
{
    public class Game
    {
        public Game()
        {
            Rat.ResetCounter();
        }

        public event EventHandler RatSpawned;
        public event EventHandler RatDied;

        public void SpawnRat(Rat rat)
        {
            RatSpawned?.Invoke(rat, EventArgs.Empty);
        }

        public void KillRat(Rat rat)
        {
            RatDied?.Invoke(rat, EventArgs.Empty);
        }
    }
}