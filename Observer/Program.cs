﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Observer
{
    internal static class Program
    {
        private static void Main()
        {
            var game = new Game();
            var rats = new List<Rat>();

            void PrintRats()
            {
                foreach (var rat in rats) Console.WriteLine(rat);

                Console.WriteLine(string.Join(string.Empty, Enumerable.Repeat("-", 20)));
            }

            using (var rat1 = new Rat(game))
            {
                rats.Add(rat1);
                PrintRats();

                using (var rat11 = new Rat(game))
                {
                    rats.Add(rat11);
                    PrintRats();

                    using (var rat111 = new Rat(game))
                    {
                        rats.Add(rat111);
                        PrintRats();
                        rats.Remove(rat111);
                    }

                    using (var rat112 = new Rat(game))
                    {
                        rats.Add(rat112);
                        PrintRats();
                        rats.Remove(rat112);
                    }

                    rats.Remove(rat11);
                }

                PrintRats();
                rats.Remove(rat1);
            }
        }
    }
}