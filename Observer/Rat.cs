﻿using System;

namespace Observer
{
    public class Rat : IDisposable
    {
        private static int _ratCount;
        private readonly Game _game;
        public int Attack = 1;

        public Rat(Game game)
        {
            _game = game;

            ++RatCount;
            game.SpawnRat(this);

            game.RatSpawned += RatEventsHandler;
            game.RatDied += RatEventsHandler;
        }

        private int RatCount
        {
            get => _ratCount;
            set
            {
                _ratCount = value;
                Attack = value;
            }
        }

        public void Dispose()
        {
            --_ratCount;
            _game.RatSpawned -= RatEventsHandler;
            _game.RatDied -= RatEventsHandler;
            _game.KillRat(this);
        }

        private void RatEventsHandler(object sender, EventArgs e)
        {
            Attack = _ratCount;
        }

        public static void ResetCounter()
        {
            _ratCount = 0;
        }

        public override string ToString()
        {
            return $"{nameof(Attack)}: {Attack}";
        }
    }
}