﻿using System;

namespace Iterator
{
    internal static class Program
    {
        private static void Main()
        {
            var node = new Node<int>(1, new Node<int>(2), new Node<int>(3));

            Console.WriteLine(string.Join(',', node.PreOrder));
        }
    }
}