﻿using System.Collections.Generic;

namespace Iterator
{
    public class Node<T>
    {
        public Node<T> Left, Right;
        public Node<T> Parent;
        public T Value;

        public Node(T value)
        {
            Value = value;
        }

        public Node(T value, Node<T> left, Node<T> right)
        {
            Value = value;
            Left = left;
            Right = right;

            left.Parent = right.Parent = this;
        }

        public IEnumerable<T> PreOrder
        {
            get
            {
                static IEnumerable<Node<T>> Traverse(Node<T> root)
                {
                    yield return root;

                    if (root.Left != null)
                        foreach (var node in Traverse(root.Left))
                            yield return node;

                    if (root.Right != null)
                        foreach (var node in Traverse(root.Right))
                            yield return node;
                }

                foreach (var node in Traverse(this)) yield return node.Value;
            }
        }
    }
}