﻿using System.Linq;

namespace Flyweight
{
    public class Sentence
    {
        private readonly string[] _plainWords;
        private readonly WordToken[] _wordTokens;

        public Sentence(string plainText)
        {
            _plainWords = plainText.Split(" ");
            _wordTokens = Enumerable
                .Range(0, _plainWords.Length)
                .Select(_ => new WordToken())
                .ToArray();
        }

        public WordToken this[int index] => _wordTokens[index];

        public override string ToString()
        {
            var words = _plainWords.Select(ToUpperSelector).ToArray();
            var fullText = string.Join(' ', words);
            return fullText;
        }

        private string ToUpperSelector(string s, int i)
        {
            return _wordTokens[i].IsCapitalized ? s.ToUpper() : s;
        }
    }
}