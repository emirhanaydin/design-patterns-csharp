﻿using System;

namespace Flyweight
{
    internal static class Program
    {
        private static void Main()
        {
            var sentence = new Sentence("hello world");
            sentence[1].IsCapitalized = true;
            Console.WriteLine(sentence);
        }
    }
}