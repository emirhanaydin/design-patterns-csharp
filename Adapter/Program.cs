﻿using System;

namespace Adapter
{
    internal static class Program
    {
        private static void Main()
        {
            var square = new Square
            {
                Side = 5
            };

            var adapter = new SquareToRectangleAdapter(square);

            Console.WriteLine(adapter);
        }
    }
}