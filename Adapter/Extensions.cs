﻿namespace Adapter
{
    public static class Extensions
    {
        public static int Area(this IRectangle rectangle)
        {
            return rectangle.Width * rectangle.Height;
        }
    }
}