﻿namespace Interpreter
{
    public class Token
    {
        public enum Type
        {
            Plus,
            Minus,
            Integer,
            Variable
        }

        public Type TokenType;
        public string ValueText;
    }
}