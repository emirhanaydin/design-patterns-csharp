﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interpreter
{
    public class Lexer
    {
        private readonly string _expression;
        private List<Token> _tokens;

        public Lexer(string expression)
        {
            _expression = expression;
        }

        public IEnumerable<Token> Lex()
        {
            if (_tokens != null) return _tokens;

            _tokens = new List<Token>();

            for (var i = 0; i < _expression.Length; i++)
            {
                var character = _expression[i];

                if (character == '+')
                {
                    _tokens.Add(new Token {TokenType = Token.Type.Plus});
                }
                else if (character == '-')
                {
                    _tokens.Add(new Token {TokenType = Token.Type.Minus});
                }
                else if (char.IsDigit(character))
                {
                    var result = new StringBuilder(character.ToString());
                    for (var j = i + 1; j < _expression.Length; j++, i++)
                    {
                        var lookForChar = _expression[j];

                        if (!char.IsDigit(lookForChar)) break;

                        result.Append(lookForChar.ToString());
                    }

                    _tokens.Add(new Token {TokenType = Token.Type.Integer, ValueText = result.ToString()});
                }
                else if (char.IsLetter(character))
                {
                    var nextChar = _expression.ElementAtOrDefault(i + 1);
                    if (char.IsLetter(nextChar))
                    {
                        _tokens = new List<Token> {new Token {TokenType = Token.Type.Integer, ValueText = "0"}};
                        return _tokens;
                    }

                    _tokens.Add(new Token {TokenType = Token.Type.Variable, ValueText = character.ToString()});
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }

            return _tokens;
        }
    }
}