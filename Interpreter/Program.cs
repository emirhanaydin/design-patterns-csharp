﻿using System;

namespace Interpreter
{
    internal static class Program
    {
        private static void Main()
        {
            var processor = new ExpressionProcessor();
            processor.Variables.Add('x', 5);
            var result = processor.Calculate("10+2+3+5+x");

            Console.WriteLine(result);
        }
    }
}