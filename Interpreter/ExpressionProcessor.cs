﻿using System;
using System.Collections.Generic;

namespace Interpreter
{
    public class ExpressionProcessor
    {
        public Dictionary<char, int> Variables = new Dictionary<char, int>();

        public int Calculate(string expression)
        {
            var lexer = new Lexer(expression);
            var tokens = lexer.Lex();
            var resolvedTokens = ResolveVariables(tokens);

            var result = Parse(resolvedTokens);

            return result.Value;
        }

        private IEnumerable<Token> ResolveVariables(IEnumerable<Token> tokens)
        {
            var result = new List<Token>();
            foreach (var token in tokens)
            {
                if (token.TokenType != Token.Type.Variable)
                {
                    result.Add(token);
                    continue;
                }

                var found = Variables.TryGetValue(token.ValueText[0], out var value);
                if (!found) return new List<Token>();

                result.Add(new Token {TokenType = Token.Type.Variable, ValueText = value.ToString()});
            }

            return result;
        }

        private IElement Parse(IEnumerable<Token> tokens)
        {
            var result = new BinaryOperation();
            var hasLhs = false;
            var hasRhs = false;

            foreach (var token in tokens)
                switch (token.TokenType)
                {
                    case Token.Type.Plus:
                        result.OperationType = BinaryOperation.Type.Addition;
                        break;
                    case Token.Type.Minus:
                        result.OperationType = BinaryOperation.Type.Subtraction;
                        break;
                    case Token.Type.Integer:
                    case Token.Type.Variable:
                        var integer = new Integer(int.Parse(token.ValueText));
                        if (hasLhs)
                        {
                            if (hasRhs) result.Left = new Integer(result.Value);

                            result.Right = integer;
                            hasRhs = true;
                        }
                        else
                        {
                            result.Left = integer;
                            hasLhs = true;
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            result.Left ??= new Integer(0);
            result.Right ??= new Integer(0);

            return result;
        }
    }
}