﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Memento
{
    public class TokenMachine
    {
        public List<Token> Tokens = new List<Token>();

        public Memento AddToken(int value)
        {
            var token = new Token(value);
            Tokens.Add(token);

            return new Memento(token);
        }

        public Memento AddToken(Token token)
        {
            return AddToken(token.Value);
        }

        public void Revert(Memento memento)
        {
            var index = Tokens.FindIndex(token => token.Value == memento.Token.Value) + 1;

            Tokens.RemoveRange(index, Tokens.Count - index);
        }

        public override string ToString()
        {
            var tokens = string.Join(Environment.NewLine, Tokens.Select((token, i) => $"\t{i}: {token.Value}"));

            return $"Tokens:{Environment.NewLine}{tokens}";
        }
    }
}