﻿using System;
using System.Collections.Generic;

namespace Memento
{
    internal static class Program
    {
        private static void Main()
        {
            var tokenMachine = new TokenMachine();
            var tokens = new List<Token>
            {
                new Token(4),
                new Token(5),
                new Token(6)
            };
            var mementos = new List<Memento>
            {
                tokenMachine.AddToken(1),
                tokenMachine.AddToken(2),
                tokenMachine.AddToken(3),
                tokenMachine.AddToken(tokens[0]),
                tokenMachine.AddToken(tokens[1]),
                tokenMachine.AddToken(tokens[2])
            };

            for (var i = 0; i < tokens.Count; i++) tokens[i].Value += i;

            tokenMachine.Revert(mementos[4]);
            Console.WriteLine(tokenMachine);
        }
    }
}