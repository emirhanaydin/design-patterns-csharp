﻿namespace Memento
{
    public class Memento
    {
        public Memento(Token token)
        {
            Token = token;
        }

        public Token Token { get; }
    }
}