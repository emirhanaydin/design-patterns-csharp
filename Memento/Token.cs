﻿namespace Memento
{
    public class Token
    {
        public int Value;

        public Token(int value)
        {
            Value = value;
        }
    }
}