﻿using System;

namespace Builder
{
    internal static class Program
    {
        private static void Main()
        {
            var builder = new CodeBuilder("Person")
                .AddField("Name", "string")
                .AddField("Age", "int");

            Console.WriteLine(builder);
        }
    }
}