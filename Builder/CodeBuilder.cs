﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    public class CodeBuilder
    {
        private const int IndentSize = 2;

        private readonly Node _rootNode;

        public CodeBuilder(string rootName)
        {
            _rootNode = new Node
            {
                Name = rootName,
                Identifiers = new[] {"public", "class"},
                Children = new List<Node>(),
                IndentLevel = 0
            };
        }

        public CodeBuilder AddField(string name, string type)
        {
            var childNode = new Node
            {
                Name = name,
                Identifiers = new[] {"public", type},
                IndentLevel = _rootNode.IndentLevel + 1
            };

            var children = _rootNode.Children;
            children.Add(childNode);

            return this;
        }

        private static string GetNodeString(Node node)
        {
            var indent = new string(' ', IndentSize * node.IndentLevel);
            var result = new StringBuilder(indent);
            var identifiersString = string.Join(" ", node.Identifiers);
            var fullName = $"{identifiersString} {node.Name}";
            result.Append(fullName);
            var children = node.Children;
            if (children == null)
            {
                result.Append(';');
                return result.ToString();
            }

            result.Append(Environment.NewLine);
            result.AppendLine("{");

            foreach (var childResult in children.Select(GetNodeString)) result.AppendLine(childResult);

            result.AppendLine("}");

            return result.ToString();
        }

        public override string ToString()
        {
            return GetNodeString(_rootNode);
        }

        private struct Node
        {
            public string[] Identifiers;
            public string Name;
            public List<Node> Children;
            public int IndentLevel;
        }
    }
}