﻿namespace Strategy
{
    public class RealDiscriminantStrategy : IDiscriminantStrategy
    {
        public double CalculateDiscriminant(double a, double b, double c)
        {
            var calculator = new DiscriminantCalculator(a, b, c);
            var discriminant = calculator.Discriminant;
            return discriminant < 0 ? double.NaN : discriminant;
        }
    }
}