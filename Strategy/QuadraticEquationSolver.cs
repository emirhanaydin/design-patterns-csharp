﻿using System;
using System.Numerics;

namespace Strategy
{
    public class QuadraticEquationSolver
    {
        private readonly IDiscriminantStrategy _strategy;

        public QuadraticEquationSolver(IDiscriminantStrategy strategy)
        {
            _strategy = strategy;
        }

        public Tuple<Complex, Complex> Solve(double a, double b, double c)
        {
            var discriminant = _strategy.CalculateDiscriminant(a, b, c);
            if (double.IsNaN(discriminant)) return new Tuple<Complex, Complex>(Complex.NaN, Complex.NaN);

            var sqrtDiscriminant = Complex.Sqrt(discriminant);

            var x1 = (-b + sqrtDiscriminant) / 2 * a;
            var x2 = (-b - sqrtDiscriminant) / 2 * a;

            return new Tuple<Complex, Complex>(x1, x2);
        }
    }
}