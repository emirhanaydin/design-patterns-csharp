﻿namespace Strategy
{
    public class OrdinaryDiscriminantStrategy : IDiscriminantStrategy
    {
        public double CalculateDiscriminant(double a, double b, double c)
        {
            var calculator = new DiscriminantCalculator(a, b, c);
            var discriminant = calculator.Discriminant;
            return discriminant;
        }
    }
}