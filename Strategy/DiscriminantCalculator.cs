﻿namespace Strategy
{
    public class DiscriminantCalculator
    {
        public DiscriminantCalculator(double a, double b, double c)
        {
            Discriminant = b * b - 4 * a * c;
        }

        public double Discriminant { get; }
    }
}