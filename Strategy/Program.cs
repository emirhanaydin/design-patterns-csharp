﻿using System;

namespace Strategy
{
    internal static class Program
    {
        private static void Main()
        {
            Console.WriteLine("Ordinary Discriminant Strategy");
            var ordinarySolver = new QuadraticEquationSolver(new OrdinaryDiscriminantStrategy());
            Console.WriteLine(ordinarySolver.Solve(1, 2, 3));
            Console.WriteLine(ordinarySolver.Solve(1, 5, 4));

            Console.WriteLine();
            Console.WriteLine("Real Discriminant Strategy");
            var realSolver = new QuadraticEquationSolver(new RealDiscriminantStrategy());
            Console.WriteLine(realSolver.Solve(1, 2, 3));
            Console.WriteLine(realSolver.Solve(1, 5, 4));
        }
    }
}