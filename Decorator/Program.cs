﻿using System;

namespace Decorator
{
    internal static class Program
    {
        private static void Main()
        {
            var dragon = new Dragon {Age = 7};

            Console.WriteLine($"Age of the dragon is {dragon.Age}");
            Console.WriteLine($"The dragon is {dragon.Fly()}");
            Console.WriteLine($"The dragon is {dragon.Crawl()}");
        }
    }
}