﻿using System;

namespace Proxy
{
    internal static class Program
    {
        private static void Main()
        {
            var person = new Person {Age = 17};
            var responsiblePerson = new ResponsiblePerson(person);

            Console.WriteLine(responsiblePerson.Drink());
            Console.WriteLine(responsiblePerson.Drive());
            Console.WriteLine(responsiblePerson.DrinkAndDrive());
        }
    }
}