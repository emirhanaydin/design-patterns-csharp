﻿namespace State
{
    public enum CombinationLockStatus
    {
        Open,
        Locked,
        Error
    }
}