﻿using System;
using System.Collections.Generic;
using System.Text;

namespace State
{
    public class CombinationLock
    {
        private readonly string _combination;
        private readonly StringBuilder _inputBuilder;

        public CombinationLock(IEnumerable<int> combination)
        {
            SetStatus(CombinationLockStatus.Locked);

            _combination = string.Join(string.Empty, combination);
            _inputBuilder = new StringBuilder(_combination.Length);
        }

        public string Status { get; private set; }

        private void SetStatus(CombinationLockStatus status)
        {
            Status = status switch
            {
                CombinationLockStatus.Open => "OPEN",
                CombinationLockStatus.Locked => "LOCKED",
                CombinationLockStatus.Error => "ERROR",
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
        }

        private void ClearInput()
        {
            _inputBuilder.Clear();
        }

        public void EnterDigit(int digit)
        {
            _inputBuilder.Append(digit);
            var input = _inputBuilder.ToString();

            if (input == _combination)
            {
                SetStatus(CombinationLockStatus.Open);
                ClearInput();
                return;
            }

            if (_combination.StartsWith(input))
            {
                Status = input;
                return;
            }

            SetStatus(CombinationLockStatus.Error);
            ClearInput();
        }

        public override string ToString()
        {
            return $"{nameof(Status)}: {Status}";
        }
    }
}