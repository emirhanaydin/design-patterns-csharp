﻿using System;

namespace State
{
    internal static class Program
    {
        private static void Main()
        {
            var combination = new[] {1, 2, 3, 4, 5};
            var combinationLock = new CombinationLock(combination);
            Console.WriteLine(combinationLock);

            for (var i = 0; i < 5; i++)
            {
                combinationLock.EnterDigit(i + 1);
                Console.WriteLine(combinationLock);
            }
        }
    }
}