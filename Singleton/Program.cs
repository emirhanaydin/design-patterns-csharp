﻿using System;

namespace Singleton
{
    internal static class Program
    {
        public static void Main()
        {
            var database = Database.Instance;
            Console.WriteLine(database.GetEntityName(1));

            static Database DatabaseFactory()
            {
                return Database.Instance;
            }

            var isSingleton = SingletonTester.IsSingleton((Func<Database>) DatabaseFactory);
            Console.WriteLine($"Is singleton: {isSingleton}");
        }
    }
}