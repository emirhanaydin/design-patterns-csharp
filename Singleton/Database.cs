﻿using System;

namespace Singleton
{
    public class Database
    {
        private static readonly Lazy<Database> LazyInstance = LazyInstance = new Lazy<Database>(() => new Database());
        public static readonly Database Instance = LazyInstance.Value;

        private Database()
        {
        }

        public string GetEntityName(int id)
        {
            return $"#{id}:{GetHashCode()}";
        }
    }
}