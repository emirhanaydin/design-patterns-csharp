﻿using System;

namespace Prototype
{
    [Serializable]
    public class Line
    {
        public Point Start, End;

        public override string ToString()
        {
            return $"{nameof(Start)}: {Start}, {nameof(End)}: {End}";
        }
    }
}