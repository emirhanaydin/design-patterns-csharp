﻿using System;

namespace Prototype
{
    internal static class Program
    {
        private static void Main()
        {
            var line1 = new Line
            {
                Start = new Point {X = 7, Y = 11},
                End = new Point {X = 9, Y = 20}
            };
            var line2 = line1.DeepCopy();
            var line3 = line2.DeepCopyXml();

            line2.Start = new Point {X = 2, Y = 5};
            line3.End = new Point {X = 3, Y = 4};

            Console.WriteLine(line1);
            Console.WriteLine(line2);
            Console.WriteLine(line3);
        }
    }
}