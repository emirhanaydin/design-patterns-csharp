﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Prototype
{
    public static class Extensions
    {
        public static T DeepCopy<T>(this T target)
        {
            using var stream = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, target);
            stream.Seek(0, SeekOrigin.Begin);
            return (T) formatter.Deserialize(stream);
        }

        public static T DeepCopyXml<T>(this T target)
        {
            var serializer = new XmlSerializer(typeof(T));
            using var stream = new MemoryStream();
            serializer.Serialize(stream, target);
            stream.Position = 0;
            return (T) serializer.Deserialize(stream);
        }
    }
}