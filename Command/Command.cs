﻿namespace Command
{
    public class Command
    {
        public enum Action
        {
            Deposit,
            Withdraw
        }

        public int Amount;
        public bool Success;

        public Action TheAction;

        public override string ToString()
        {
            return $"{nameof(TheAction)}: {TheAction}, {nameof(Amount)}: {Amount}, {nameof(Success)}: {Success}";
        }
    }
}