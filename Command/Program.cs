﻿using System;
using System.Collections.Generic;

namespace Command
{
    internal static class Program
    {
        private static void Main()
        {
            var commands = new List<Command>
            {
                new Command {TheAction = Command.Action.Deposit, Amount = 100},
                new Command {TheAction = Command.Action.Withdraw, Amount = 50},
                new Command {TheAction = Command.Action.Withdraw, Amount = 50},
                new Command {TheAction = Command.Action.Withdraw, Amount = 50}
            };

            var account = new Account();

            foreach (var command in commands)
            {
                account.Process(command);
                Console.WriteLine(command);
                Console.WriteLine(account);
            }
        }
    }
}