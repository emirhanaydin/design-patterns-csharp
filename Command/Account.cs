﻿using System;

namespace Command
{
    public class Account
    {
        public int Balance { get; set; }

        public void Process(Command command)
        {
            switch (command.TheAction)
            {
                case Command.Action.Deposit:
                    Deposit(command.Amount);
                    command.Success = true;
                    break;
                case Command.Action.Withdraw:
                    command.Success = Withdraw(command.Amount);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Deposit(int amount)
        {
            Balance += amount;
        }

        private bool Withdraw(int amount)
        {
            var newBalance = Balance - amount;
            if (newBalance < 0) return false;

            Balance = newBalance;
            return true;
        }

        public override string ToString()
        {
            return $"{nameof(Balance)}: {Balance}";
        }
    }
}