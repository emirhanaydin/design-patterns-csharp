﻿using System.Collections.Generic;
using System.Text;

namespace ChainOfResponsibility
{
    public class Game
    {
        public readonly IList<Creature> Creatures = new List<Creature>();

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            foreach (var creature in Creatures) stringBuilder.AppendLine(creature.ToString());

            return stringBuilder.ToString();
        }
    }
}