﻿using System;

namespace ChainOfResponsibility
{
    internal static class Program
    {
        private static void Main()
        {
            var game = new Game();

            void AddGoblin()
            {
                game.Creatures.Add(new Goblin(game));
            }

            void AddGoblinKing()
            {
                game.Creatures.Add(new GoblinKing(game));
            }

            AddGoblin();
            AddGoblin();
            AddGoblin();
            AddGoblinKing();

            Console.WriteLine(game);
        }
    }
}