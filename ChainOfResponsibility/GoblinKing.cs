﻿namespace ChainOfResponsibility
{
    public class GoblinKing : Goblin
    {
        public GoblinKing(Game game) : base(game)
        {
        }

        protected override int BaseAttack => 3;
        protected override int BaseDefense => 3;
    }
}