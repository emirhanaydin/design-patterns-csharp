﻿using System.Linq;

namespace ChainOfResponsibility
{
    public class Goblin : Creature
    {
        public Goblin(Game game) : base(game)
        {
        }

        protected virtual int BaseAttack => 1;
        protected virtual int BaseDefense => 1;

        public override int Attack => BaseAttack + GetAttackModifier();

        public override int Defense => BaseDefense + GetDefenceModifier();

        private int GetAttackModifier()
        {
            var goblinKings = Game.Creatures.Where(creature => creature is GoblinKing && creature != this);

            return goblinKings.Count();
        }

        private int GetDefenceModifier()
        {
            var goblins = Game.Creatures.Where(creature => creature is Goblin && creature != this);

            return goblins.Count();
        }
    }
}