﻿namespace ChainOfResponsibility
{
    public abstract class Creature
    {
        protected readonly Game Game;

        protected Creature(Game game)
        {
            Game = game;
        }

        public abstract int Attack { get; }
        public abstract int Defense { get; }

        public override string ToString()
        {
            return $"{nameof(Attack)}: {Attack}, {nameof(Defense)}: {Defense}";
        }
    }
}