﻿# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.23] - 2021-03-26

### Added

- Visitor pattern example project.

## [0.0.22] - 2021-03-26

### Added

- Template Method pattern example project.

## [0.0.21] - 2021-03-25

### Added

- Strategy pattern example project.

## [0.0.20] - 2021-03-25

### Added

- State pattern example project.

## [0.0.19] - 2021-03-25

### Added

- Observer pattern example project.

## [0.0.18] - 2021-03-24

### Added

- Null Object pattern example project.

## [0.0.17] - 2021-03-24

### Added

- Memento pattern example project.

## [0.0.16] - 2021-03-23

### Added

- Mediator pattern example project.

## [0.0.15] - 2021-03-23

### Added

- Iterator pattern example project.

## [0.0.14] - 2021-03-23

### Added

- Interpreter pattern example project.

## [0.0.13] - 2021-03-22

### Added

- Command pattern example project.

## [0.0.12] - 2021-03-22

### Added

- Chain of Responsibility pattern example project.

## [0.0.11] - 2021-03-20

### Added

- Proxy pattern example project.

## [0.0.10] - 2021-03-20

### Added

- Flyweight pattern example project.

## [0.0.9] - 2021-03-18

### Added

- Facade pattern example project.

## [0.0.8] - 2021-03-18

### Added

- Decorator pattern example project.

## [0.0.7] - 2021-03-18

### Added

- Composite pattern example project.

## [0.0.6] - 2021-03-17

### Added

- Bridge pattern example project.

## [0.0.5] - 2021-03-16

### Added

- Adapter pattern example project.

## [0.0.4] - 2021-03-16

### Added

- Singleton pattern example project.

## [0.0.3] - 2021-03-15

### Added

- Prototype pattern example project.

## [0.0.2] - 2021-03-13

### Added

- Factory pattern example project.

## [0.0.1] - 2021-03-13

### Added

- Builder pattern example project.

[unreleased]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/compare/0.0.23...master
[0.0.23]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.23
[0.0.22]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.22
[0.0.21]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.21
[0.0.20]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.20
[0.0.19]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.19
[0.0.18]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.18
[0.0.17]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.17
[0.0.16]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.16
[0.0.15]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.15
[0.0.14]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.14
[0.0.13]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.13
[0.0.12]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.12
[0.0.11]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.11
[0.0.10]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.10
[0.0.9]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.9
[0.0.8]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.8
[0.0.7]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.7
[0.0.6]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.6
[0.0.5]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.5
[0.0.4]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.4
[0.0.3]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.3
[0.0.2]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.2
[0.0.1]: https://gitlab.com/emirhanaydin/design-patterns-csharp/-/tags/0.0.1
