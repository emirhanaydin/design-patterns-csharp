﻿namespace TemplateMethod
{
    public abstract class CardGame
    {
        public Creature[] Creatures;

        protected CardGame(Creature[] creatures)
        {
            Creatures = creatures;
        }

        public int Combat(int creature1Idx, int creature2Idx)
        {
            var creature1 = Creatures[creature1Idx];
            var creature2 = Creatures[creature2Idx];

            Hit(creature1, creature2);
            Hit(creature2, creature1);

            var isCreature1Alive = creature1.Health > 0;
            var isCreature2Alive = creature2.Health > 0;

            // Both are dead, no winner
            if (isCreature1Alive == isCreature2Alive) return -1;

            return isCreature1Alive ? creature1Idx : creature2Idx;
        }

        protected abstract void Hit(Creature attacker, Creature target);
    }
}