﻿namespace TemplateMethod
{
    public class TemporaryCardDamageGame : CardGame
    {
        public TemporaryCardDamageGame(Creature[] creatures) : base(creatures)
        {
        }

        protected override void Hit(Creature attacker, Creature target)
        {
            var healthLeft = target.Health - attacker.Attack;

            if (healthLeft <= 0) target.Health = healthLeft;
        }
    }
}