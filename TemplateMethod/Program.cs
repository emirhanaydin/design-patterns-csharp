﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TemplateMethod
{
    internal static class Program
    {
        private static void Main()
        {
            var creatureMatches = new[]
            {
                new[] {new Creature(1, 2), new Creature(1, 2)},
                new[] {new Creature(1, 1), new Creature(2, 2)},
                new[] {new Creature(2, 2), new Creature(2, 2)},
                new[] {new Creature(1, 2), new Creature(1, 3)}
            };

            for (var i = 0; i < creatureMatches.Length; i++)
            {
                var creatures = creatureMatches[i];
                var tempCreatures = CloneCreatures(creatures);
                var permanentCreatures = CloneCreatures(creatures);

                var temporaryGame = new TemporaryCardDamageGame(tempCreatures);
                var permanentGame = new PermanentCardDamageGame(permanentCreatures);

                var permanentWinner = -1;
                while (permanentCreatures[0].Health > 0 && permanentCreatures[1].Health > 0)
                    permanentWinner = permanentGame.Combat(0, 1);

                Console.WriteLine($"#{i + 1} Temporary game: {GetWinnerMessage(temporaryGame.Combat(0, 1))}");
                Console.WriteLine($"#{i + 1} Permanent game: {GetWinnerMessage(permanentWinner)}");
                Console.WriteLine();
            }
        }

        private static string GetWinnerMessage(int winnerIdx)
        {
            return winnerIdx > 0 ? $"winner is {winnerIdx}" : "no winner";
        }

        private static Creature[] CloneCreatures(IEnumerable<Creature> creatures)
        {
            return creatures.Select(creature => new Creature(creature.Attack, creature.Health)).ToArray();
        }
    }
}