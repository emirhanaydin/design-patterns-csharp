﻿namespace TemplateMethod
{
    public class PermanentCardDamageGame : CardGame
    {
        public PermanentCardDamageGame(Creature[] creatures) : base(creatures)
        {
        }

        protected override void Hit(Creature attacker, Creature target)
        {
            target.Health -= attacker.Attack;
        }
    }
}