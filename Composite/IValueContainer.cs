﻿using System.Collections.Generic;

namespace Composite
{
    public interface IValueContainer : IEnumerable<int>
    {
    }
}