﻿using System.Collections.Generic;
using System.Linq;

namespace Composite
{
    public static class Extensions
    {
        public static int Sum(this IEnumerable<IValueContainer> containers)
        {
            return containers.Sum(container => container.Sum());
        }
    }
}