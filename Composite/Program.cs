﻿using System;
using System.Collections.Generic;

namespace Composite
{
    internal static class Program
    {
        private static void Main()
        {
            var many = new ManyValues {1, 2, 3};
            var single1 = new SingleValue {Value = 100};
            var single2 = new SingleValue {Value = 200};

            var containers = new List<IValueContainer>
            {
                many,
                single1,
                single2
            };

            var sum = containers.Sum();

            Console.WriteLine(sum);
        }
    }
}