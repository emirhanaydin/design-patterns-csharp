﻿using System;

namespace NullObject
{
    public class Account
    {
        private readonly ILog _log;

        public Account(ILog log)
        {
            _log = log;
        }

        public void SomeOperation()
        {
            var count = _log.RecordCount;
            _log.LogInfo("Performing an operation");

            if (count + 1 != _log.RecordCount) throw new Exception();
            if (_log.RecordCount >= _log.RecordLimit) throw new Exception();
        }
    }
}