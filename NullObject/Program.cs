﻿namespace NullObject
{
    internal static class Program
    {
        private static void Main()
        {
            var log = new NullLog();
            var account = new Account(log);

            account.SomeOperation();
        }
    }
}