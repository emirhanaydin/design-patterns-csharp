﻿namespace Bridge
{
    public class Square : Shape
    {
        public Square(IRenderer renderer) : base(renderer)
        {
            Name = "Square";
        }

        public override string ToString()
        {
            return $"Drawing {Name} as {Renderer.WhatToRenderAs}";
        }
    }
}