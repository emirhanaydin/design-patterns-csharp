﻿namespace Bridge
{
    public class Triangle : Shape
    {
        public Triangle(IRenderer renderer) : base(renderer)
        {
            Name = "Triangle";
        }

        public override string ToString()
        {
            return $"Drawing {Name} as {Renderer.WhatToRenderAs}";
        }
    }
}