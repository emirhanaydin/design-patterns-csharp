﻿using System;

namespace Bridge
{
    internal static class Program
    {
        private static void Main()
        {
            var square = new Square(new RasterRenderer());
            Console.WriteLine(square.ToString());
        }
    }
}