﻿using System.Collections.Generic;

namespace Facade
{
    public class MagicSquareGenerator
    {
        public List<List<int>> Generate(int size)
        {
            var generator = new Generator();
            var splitter = new Splitter();
            var verifier = new Verifier();

            List<List<int>> result;
            do
            {
                var buffer = new List<List<int>>(size);

                for (var i = 0; i < size; i++)
                {
                    var squares = generator.Generate(size);
                    buffer.Add(squares);
                }

                result = splitter.Split(buffer);
            } while (!verifier.Verify(result));

            return result;
        }
    }
}