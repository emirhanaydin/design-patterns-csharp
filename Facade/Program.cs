﻿using System;

namespace Facade
{
    internal static class Program
    {
        private static void Main()
        {
            var magicSquareGenerator = new MagicSquareGenerator();
            var magicSquares = magicSquareGenerator.Generate(4);

            magicSquares.ForEach(row =>
            {
                row.ForEach(Console.Write);
                Console.WriteLine();
            });
        }
    }
}