﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Facade
{
    public class Generator
    {
        private static readonly Random RandomInstance = new Random();

        public List<int> Generate(int count)
        {
            return Enumerable.Range(0, count)
                .Select(_ => RandomInstance.Next(1, 6))
                .ToList();
        }
    }
}