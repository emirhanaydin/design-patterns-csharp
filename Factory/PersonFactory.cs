﻿namespace Factory
{
    public class PersonFactory
    {
        private int _id;

        public Person CreatePerson(string name)
        {
            return new Person
            {
                Name = name,
                Id = _id++
            };
        }
    }
}