﻿using System;

namespace Factory
{
    internal static class Program
    {
        private static void Main()
        {
            var personFactory = new PersonFactory();

            Console.WriteLine(personFactory.CreatePerson("First User"));
            Console.WriteLine(personFactory.CreatePerson("Second User"));
            Console.WriteLine(personFactory.CreatePerson("Third User"));
            Console.WriteLine(personFactory.CreatePerson("Fourth User"));
            Console.WriteLine(personFactory.CreatePerson("Fifth User"));
        }
    }
}